from bokeh.layouts import row, column, widgetbox
from bokeh.models import TextInput, Button,  Div
from bokeh.plotting import curdoc
import pandas as pd
import numpy as np
from arvore_decisao import carregar_modelo_aprendizado
from arvore_decisao import predicao

def predicao_arvore():

  valores_passados_pelo_cliente=np.array([
  float(idade.value), 
  float(numero_vertebras.value), 
  float(vertebra_inicial.value)])

  resultado_predicao = predicao(valores_passados_pelo_cliente, modelo_aprendizado)

  try:
    if resultado_predicao == 'absent':
      resultado_exame='foi eficiente'
    else:
      resultado_exame='não foi eficiente'

    resultado ='<br>O processo cirúrgico {}'.format(resultado_exame)

    div_widget.text = resultado

  except KeyboardInterrupt:
    return 
  except ValueError as e:
    div_widget.text = "<h2>Um erro ocorreu: {}</h2>".format(str(e))
  except Exception as e:
    errMsg = "<h2>Ocorreu um erro inesperado com a mensagem de erro: <br>"
    div_widget.text = errMsg

modelo_aprendizado=carregar_modelo_aprendizado()


idade = TextInput(title='Qual a idade em meses ?', value="")
numero_vertebras = TextInput(title='Número de vértebras na qual houve intervenção   ', value="")
vertebra_inicial = TextInput(title='Qual vértebra o processo foi iniciado', value="")

div_widget = Div(text="", width=400, height=100)

button_widget = Button(label="Predição", button_type="success")
button_widget.on_click(predicao_arvore)  

controls = widgetbox([
idade, numero_vertebras, vertebra_inicial, 
button_widget], width=200)

layout = row(column(controls), column(div_widget))

curdoc().add_root(layout)

curdoc().title = "Árvore de Decisão com Árvore de Decisão"    
