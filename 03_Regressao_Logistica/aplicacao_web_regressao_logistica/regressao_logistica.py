import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression

def carregar_modelo():
    ad_data = pd.read_csv('advertising.csv')
    X = ad_data[['Daily Time Spent on Site', 'Age', 'Area Income','Daily Internet Usage', 'Male']]
    y = ad_data['Clicked on Ad']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
    logmodel = LogisticRegression()
    logmodel.fit(X_train, y_train)
    
    return logmodel

def predicao(valores_passados_pelo_cliente, modelo):
    res_predicao = modelo.predict(valores_passados_pelo_cliente.reshape(1,-1))
    return (res_predicao)
