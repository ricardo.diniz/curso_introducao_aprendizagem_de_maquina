from bokeh.layouts import row, column, widgetbox
from bokeh.models import TextInput, Button,  Div
from bokeh.plotting import curdoc
import pandas as pd
import numpy as np
from regressao_logistica import carregar_modelo
from regressao_logistica import predicao

def predicao_regressao_logistica():
    
  valores_passados_pelo_cliente=np.array([float(tempoNoSiteMinutos.value),float(idade.value),float(mediaRendaConsumidor.value),float(minutosDia.value),float(masculino.value)])
  res_predicao = predicao(valores_passados_pelo_cliente, modelo_regressao_logistica)

  try:
    if res_predicao == 1:
      clicou='clicou'
    else:
      clicou='não clicou'

    resultado ='<br>O usuário {} no anúnico'.format(clicou)

    div_widget.text = resultado

  except KeyboardInterrupt:
    return 
  except ValueError as e:
    div_widget.text = "<h2>Um erro ocorreu: {}</h2>".format(str(e))
  except Exception as e:
    errMsg = "<h2>Ocorreu um erro inesperado com a mensagem de erro: <br>"
    div_widget.text = errMsg

modelo_regressao_logistica=carregar_modelo()

tempoNoSiteMinutos = TextInput(title="tempo no site em minutos", value="")
idade = TextInput(title="idade do consumidor", value="")
mediaRendaConsumidor = TextInput(title="Média da renda do consumidor", value="")
minutosDia = TextInput(title="minutos por dia que o consumidor está na internet", value="")
masculino = TextInput(title="Masculino 1-Sim 0-Não", value="")

div_widget = Div(text="", width=400, height=100)

button_widget = Button(label="Análise", button_type="success")
button_widget.on_click(predicao_regressao_logistica)  

controls = widgetbox([tempoNoSiteMinutos, idade, mediaRendaConsumidor, minutosDia, masculino, button_widget], width=200)

layout = row(column(controls), column(div_widget))

curdoc().add_root(layout)
curdoc().title = "Regressão Logística"    
