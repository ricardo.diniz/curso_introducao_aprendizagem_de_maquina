from flask import Flask
import pandas as pd
import numpy as np
from regressao_logistica import carregar_modelo
from regressao_logistica import predicao

app = Flask(__name__)

modelo_regressao_logistica=carregar_modelo()


# https://www.mercadolivre.com.br/menu/departments

@app.route("/predicao/<tempoNoSiteMinutos>/<idade>/<mediaRendaConsumidor>/<minutosDia>/<masculino>")
def predict(tempoNoSiteMinutos=0, idade=0, mediaRendaConsumidor=0, minutosDia=0, masculino=0):
    valores_passados_pelo_cliente=np.array([float(tempoNoSiteMinutos),float(idade),float(mediaRendaConsumidor),float(minutosDia),float(masculino)])
    res_predicao = predicao(valores_passados_pelo_cliente, modelo_regressao_logistica)

    if res_predicao == 1:
        clicou='clicou'
    else:
        clicou='não clicou'

    resultado ='<br>O usuário {} no anúnico'.format(clicou)

    return resultado

if __name__ == "__main__":    
    app.run(debug=True)

#curl  http://127.0.0.1:5000/predicao/1/1/1/131/1
#curl http://127.0.0.1:5000/predicao/66/48/24593/131/1
