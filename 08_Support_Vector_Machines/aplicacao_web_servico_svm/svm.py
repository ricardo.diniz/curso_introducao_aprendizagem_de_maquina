from sklearn.datasets import load_breast_cancer
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV

def carregar_modelo():
    cancer = load_breast_cancer()
    df_feat = pd.DataFrame(cancer['data'],columns=cancer['feature_names'])
    df_target = pd.DataFrame(cancer['target'],columns=['Cancer'])
    X_train, X_test, y_train, y_test = train_test_split(df_feat, np.ravel(df_target), test_size=0.30, random_state=101)
    modelo_svm = SVC(C=10, gamma=0.0001, kernel='rbf')
    modelo_svm.fit(X_train,y_train)

# Otimização de hiperparâmetros com GridSearch
#    param_grid = {'C': [0.1,1, 10, 100, 1000], 'gamma': [1,0.1,0.01,0.001,0.0001], 'kernel': ['rbf']} 
#    modelo_svm_hiperparametros_otimizados = GridSearchCV(modelo_svm, param_grid, refit=True,verbose=3)
#    modelo_svm_hiperparametros_otimizados.fit(X_train,y_train)
#    return modelo_svm_hiperparametros_otimizados
    
    return modelo_svm

def predicao(valores_passados_pelo_cliente, modelo_svm):
    resposta_predicao = modelo_svm.predict(valores_passados_pelo_cliente.reshape(1,-1))
    return (resposta_predicao)
