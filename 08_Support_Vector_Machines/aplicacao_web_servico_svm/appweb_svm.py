from bokeh.layouts import row, column, widgetbox
from bokeh.models import TextInput, Button,  Div
from bokeh.plotting import curdoc
import pandas as pd
import numpy as np
from svm import carregar_modelo
from svm import predicao

def predicao_svm():

  valores_passados_pelo_cliente=np.array([
  float(mean_radius.value), 
  float(mean_texture.value), 
  float(mean_perimeter.value),
  float(mean_area.value), 
  float(mean_smoothness.value),
  float(mean_compactness.value),
  float(mean_concavity.value),
  float(mean_concave_points.value), 
  float(mean_symmetry.value),
  float(mean_fractal_dimension.value), 
  float(radius_error.value),
  float(texture_error.value), 
  float(perimeter_error.value),
  float(area_error.value), 
  float(smoothness_error.value),
  float(compactness_error.value), 
  float(concavity_error.value),
  float(concave_points_error.value), 
  float(symmetry_error.value),
  float(fractal_dimension_error.value), 
  float(worst_radius.value),
  float(worst_texture.value), 
  float(worst_perimeter.value),
  float(worst_area.value), 
  float(worst_smoothness.value), 
  float(worst_compactness.value), 
  float(worst_concavity.value), 
  float(worst_concave_points.value), 
  float(worst_symmetry.value), 
  float(worst_fractal_dimension.value)])
  resultado_predicao = predicao(valores_passados_pelo_cliente, modelo_svm)

  try:
    if resultado_predicao == 1:
      tumor='maligno'
    else:
      tumor='benigno'

    resultado ='<br>O diagnóstico constatou que o tumor é {}'.format(tumor)

    div_widget.text = resultado

  except KeyboardInterrupt:
    return 
  except ValueError as e:
    div_widget.text = "<h2>Um erro ocorreu: {}</h2>".format(str(e))
  except Exception as e:
    errMsg = "<h2>Ocorreu um erro inesperado com a mensagem de erro: <br>"
    div_widget.text = errMsg

modelo_svm=carregar_modelo()


mean_radius = TextInput(title='mean radius', value="13.54")
mean_texture = TextInput(title='mean texture', value="14.36")
mean_perimeter = TextInput(title='mean perimeter', value="14.36")
mean_area = TextInput(title='mean area', value="566.3")
mean_smoothness = TextInput(title='mean smoothness', value="0.09779")
mean_compactness= TextInput(title='mean compactness', value="0.08129")
mean_concavity=TextInput(title='mean concavity', value="0.06664")
mean_concave_points=TextInput(title='mean concave points', value="0.04781")
mean_symmetry=TextInput(title='mean symmetry', value="0.1885")
mean_fractal_dimension=TextInput(title='mean fractal dimension',value="0.05766")
radius_error=TextInput(title="radius error", value="0.2699") 
texture_error=TextInput(title='texture error', value="0.7886")
perimeter_error=TextInput(title='perimeter error', value="2.058")
area_error=TextInput(title='area error',value="23.56")
smoothness_error=TextInput(title='smoothness error', value="0.008462")
compactness_error=TextInput(title='compactness error', value="0.0146")
concavity_error=TextInput(title='concavity error',value="0.02387")
concave_points_error=TextInput(title='concave points error', value="0.01315")
symmetry_error=TextInput(title='symmetry error',value="0.0198")
fractal_dimension_error=TextInput(title='fractal dimension error',value="0.0023") 
worst_radius=TextInput(title='worst radius', value="15.11")
worst_texture=TextInput(title='worst texture',value="19.26")
worst_perimeter=TextInput(title='worst perimeter', value="99.7")
worst_area=TextInput(title='worst area', value="711.2")
worst_smoothness=TextInput(title='worst smoothness',value="0.144")
worst_compactness=TextInput(title='worst compactness', value="0.1773")
worst_concavity=TextInput(title='worst concavity', value="0.239")
worst_concave_points=TextInput(title='worst concave points',value="0.1288")
worst_symmetry=TextInput(title='worst symmetry', value="0.2977")
worst_fractal_dimension=TextInput(title='worst fractal dimension', value="0.07259")

div_widget = Div(text="", width=400, height=100)

button_widget = Button(label="Predição", button_type="success")
button_widget.on_click(predicao_svm)  

controls = widgetbox([
mean_radius, mean_texture, mean_perimeter,
mean_area, mean_smoothness,
mean_compactness,mean_concavity,
mean_concave_points, mean_symmetry,
mean_fractal_dimension, radius_error,
texture_error, perimeter_error,
area_error, smoothness_error,
compactness_error, concavity_error,
concave_points_error, symmetry_error,
fractal_dimension_error, worst_radius,
worst_texture, worst_perimeter,
worst_area, worst_smoothness, 
worst_compactness, worst_concavity, 
worst_concave_points, worst_symmetry, worst_fractal_dimension, 
button_widget], width=200)

layout = row(column(controls), column(div_widget))

curdoc().add_root(layout)

curdoc().title = "SVM"    
